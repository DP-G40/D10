package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Customer;
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {



}
