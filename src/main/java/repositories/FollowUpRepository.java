package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.FollowUp;
@Repository
public interface FollowUpRepository extends JpaRepository<FollowUp, Integer> {



}
