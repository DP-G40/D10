package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Newspaper extends DomainEntity {

    // Atributos ----
    private String title;
    private Date publicationDate;
    private String description;
    private Boolean draft = true;
    private Boolean private =false;

    // Constructor ----

    public Newspaper() {
        super();
    }

}
