package domain;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
@Entity
@Access(AccessType.PROPERTY)
public class Actor extends DomainEntity {
 
 // Atributos ---- 
private String name;
private String surname;
private String direction;
private String phoneNumber;
private String email;
 
 // Constructor ---- 
 
 public Actor(){
 super();
}
 
}
