package domain;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Article extends DomainEntity {
 
 // Atributos ---- 
private String title;
private Date publicationDate;
private String summary;
private String body;
private Boolean draft;
 
 // Constructor ---- 
 
 public Article(){
 super();
}
 
}
