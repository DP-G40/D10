package domain;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
@Entity
@Access(AccessType.PROPERTY)
public class Chirp extends DomainEntity {
 
 // Atributos ---- 
private String title;
private String description;
private String publicationDate;
 
 // Constructor ---- 
 
 public Chirp(){
 super();
}
 
}
