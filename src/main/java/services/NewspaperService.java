package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.NewspaperRepository;
import domain.Newspaper;

@Service
@Transactional
public class NewspaperService {
// Managed repository -----------------------------------------------------
@Autowired
private NewspaperRepository newspaperRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public NewspaperService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Newspaper create() {
Newspaper result;
result = new Newspaper();
return result;
}

public Collection<Newspaper> findAll() {
Collection<Newspaper> result;
Assert.notNull(newspaperRepository);
result = newspaperRepository.findAll();
Assert.notNull(result);
return result;
}

public Newspaper findOne(int newspaperId) {
Newspaper result;
result = newspaperRepository.findOne(newspaperId);
return result;
}

public Newspaper save(Newspaper newspaper) {
Assert.notNull(newspaper);
Newspaper result;
result = newspaperRepository.save(newspaper);
return result;
}

public void delete(Newspaper newspaper) {
Assert.notNull(newspaper);
Assert newspaper.getId() != 0;
newspaperRepository.delete(newspaper);
}

// Other business methods -------------------------------------------------
}
