package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.FollowUpRepository;
import domain.FollowUp;

@Service
@Transactional
public class FollowUpService {
// Managed repository -----------------------------------------------------
@Autowired
private FollowUpRepository followupRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public FollowUpService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public FollowUp create() {
FollowUp result;
result = new FollowUp();
return result;
}

public Collection<FollowUp> findAll() {
Collection<FollowUp> result;
Assert.notNull(followupRepository);
result = followupRepository.findAll();
Assert.notNull(result);
return result;
}

public FollowUp findOne(int followupId) {
FollowUp result;
result = followupRepository.findOne(followupId);
return result;
}

public FollowUp save(FollowUp followup) {
Assert.notNull(followup);
FollowUp result;
result = followupRepository.save(followup);
return result;
}

public void delete(FollowUp followup) {
Assert.notNull(followup);
Assert followup.getId() != 0;
followupRepository.delete(followup);
}

// Other business methods -------------------------------------------------
}
