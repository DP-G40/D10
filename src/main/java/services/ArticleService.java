package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.ArticleRepository;
import domain.Article;

@Service
@Transactional
public class ArticleService {
// Managed repository -----------------------------------------------------
@Autowired
private ArticleRepository articleRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public ArticleService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Article create() {
Article result;
result = new Article();
return result;
}

public Collection<Article> findAll() {
Collection<Article> result;
Assert.notNull(articleRepository);
result = articleRepository.findAll();
Assert.notNull(result);
return result;
}

public Article findOne(int articleId) {
Article result;
result = articleRepository.findOne(articleId);
return result;
}

public Article save(Article article) {
Assert.notNull(article);
Article result;
result = articleRepository.save(article);
return result;
}

public void delete(Article article) {
Assert.notNull(article);
Assert article.getId() != 0;
articleRepository.delete(article);
}

// Other business methods -------------------------------------------------
}
