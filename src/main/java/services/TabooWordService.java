package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.TabooWordRepository;
import domain.TabooWord;

@Service
@Transactional
public class TabooWordService {
// Managed repository -----------------------------------------------------
@Autowired
private TabooWordRepository taboowordRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public TabooWordService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public TabooWord create() {
TabooWord result;
result = new TabooWord();
return result;
}

public Collection<TabooWord> findAll() {
Collection<TabooWord> result;
Assert.notNull(taboowordRepository);
result = taboowordRepository.findAll();
Assert.notNull(result);
return result;
}

public TabooWord findOne(int taboowordId) {
TabooWord result;
result = taboowordRepository.findOne(taboowordId);
return result;
}

public TabooWord save(TabooWord tabooword) {
Assert.notNull(tabooword);
TabooWord result;
result = taboowordRepository.save(tabooword);
return result;
}

public void delete(TabooWord tabooword) {
Assert.notNull(tabooword);
Assert tabooword.getId() != 0;
taboowordRepository.delete(tabooword);
}

// Other business methods -------------------------------------------------
}
