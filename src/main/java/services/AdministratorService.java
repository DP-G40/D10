package services;

import domain.Administrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AdministratorRepository;

import java.util.Collection;

@Service
@Transactional
public class AdministratorService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private AdministratorRepository administratorRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public AdministratorService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Administrator create() {
        Administrator result;
        result = new Administrator();
        return result;
    }

    public Collection<Administrator> findAll() {
        Collection<Administrator> result;
        Assert.notNull(administratorRepository);
        result = administratorRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Administrator findOne(int administratorId) {
        Administrator result;
        result = administratorRepository.findOne(administratorId);
        return result;
    }

    public Administrator save(Administrator administrator) {
        Assert.notNull(administrator);
        Administrator result;
        result = administratorRepository.save(administrator);
        return result;
    }

    public void delete(Administrator administrator) {
        Assert.notNull(administrator);
        Assert administrator.getId() != 0;
        administratorRepository.delete(administrator);
    }

// Other business methods -------------------------------------------------
}
