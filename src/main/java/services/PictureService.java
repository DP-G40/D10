package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.PictureRepository;
import domain.Picture;

@Service
@Transactional
public class PictureService {
// Managed repository -----------------------------------------------------
@Autowired
private PictureRepository pictureRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public PictureService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Picture create() {
Picture result;
result = new Picture();
return result;
}

public Collection<Picture> findAll() {
Collection<Picture> result;
Assert.notNull(pictureRepository);
result = pictureRepository.findAll();
Assert.notNull(result);
return result;
}

public Picture findOne(int pictureId) {
Picture result;
result = pictureRepository.findOne(pictureId);
return result;
}

public Picture save(Picture picture) {
Assert.notNull(picture);
Picture result;
result = pictureRepository.save(picture);
return result;
}

public void delete(Picture picture) {
Assert.notNull(picture);
Assert picture.getId() != 0;
pictureRepository.delete(picture);
}

// Other business methods -------------------------------------------------
}
