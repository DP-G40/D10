package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.ActorRepository;
import domain.Actor;

@Service
@Transactional
public class ActorService {
// Managed repository -----------------------------------------------------
@Autowired
private ActorRepository actorRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public ActorService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Actor create() {
Actor result;
result = new Actor();
return result;
}

public Collection<Actor> findAll() {
Collection<Actor> result;
Assert.notNull(actorRepository);
result = actorRepository.findAll();
Assert.notNull(result);
return result;
}

public Actor findOne(int actorId) {
Actor result;
result = actorRepository.findOne(actorId);
return result;
}

public Actor save(Actor actor) {
Assert.notNull(actor);
Actor result;
result = actorRepository.save(actor);
return result;
}

public void delete(Actor actor) {
Assert.notNull(actor);
Assert actor.getId() != 0;
actorRepository.delete(actor);
}

// Other business methods -------------------------------------------------
}
