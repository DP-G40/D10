package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.SubscriptionRepository;
import domain.Subscription;

@Service
@Transactional
public class SubscriptionService {
// Managed repository -----------------------------------------------------
@Autowired
private SubscriptionRepository subscriptionRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public SubscriptionService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Subscription create() {
Subscription result;
result = new Subscription();
return result;
}

public Collection<Subscription> findAll() {
Collection<Subscription> result;
Assert.notNull(subscriptionRepository);
result = subscriptionRepository.findAll();
Assert.notNull(result);
return result;
}

public Subscription findOne(int subscriptionId) {
Subscription result;
result = subscriptionRepository.findOne(subscriptionId);
return result;
}

public Subscription save(Subscription subscription) {
Assert.notNull(subscription);
Subscription result;
result = subscriptionRepository.save(subscription);
return result;
}

public void delete(Subscription subscription) {
Assert.notNull(subscription);
Assert subscription.getId() != 0;
subscriptionRepository.delete(subscription);
}

// Other business methods -------------------------------------------------
}
