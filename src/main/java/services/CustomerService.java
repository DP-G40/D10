package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.CustomerRepository;
import domain.Customer;

@Service
@Transactional
public class CustomerService {
// Managed repository -----------------------------------------------------
@Autowired
private CustomerRepository customerRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public CustomerService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public Customer create() {
Customer result;
result = new Customer();
return result;
}

public Collection<Customer> findAll() {
Collection<Customer> result;
Assert.notNull(customerRepository);
result = customerRepository.findAll();
Assert.notNull(result);
return result;
}

public Customer findOne(int customerId) {
Customer result;
result = customerRepository.findOne(customerId);
return result;
}

public Customer save(Customer customer) {
Assert.notNull(customer);
Customer result;
result = customerRepository.save(customer);
return result;
}

public void delete(Customer customer) {
Assert.notNull(customer);
Assert customer.getId() != 0;
customerRepository.delete(customer);
}

// Other business methods -------------------------------------------------
}
