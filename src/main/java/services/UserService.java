package services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.util.Collection;
import java.util.Date;
import repositories.UserRepository;
import domain.User;

@Service
@Transactional
public class UserService {
// Managed repository -----------------------------------------------------
@Autowired
private UserRepository userRepository;
// Suporting repository --------------------------------------------------

// Constructors -----------------------------------------------------------
public UserService() {
super();
}
// Simple CRUD methods ----------------------------------------------------
public User create() {
User result;
result = new User();
return result;
}

public Collection<User> findAll() {
Collection<User> result;
Assert.notNull(userRepository);
result = userRepository.findAll();
Assert.notNull(result);
return result;
}

public User findOne(int userId) {
User result;
result = userRepository.findOne(userId);
return result;
}

public User save(User user) {
Assert.notNull(user);
User result;
result = userRepository.save(user);
return result;
}

public void delete(User user) {
Assert.notNull(user);
Assert user.getId() != 0;
userRepository.delete(user);
}

// Other business methods -------------------------------------------------
}
