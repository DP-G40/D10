package converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import domain.FollowUp;
@Component
@Transactional
public class FollowUpToStringConverter implements Converter<FollowUp, String> {@Override
public String convert(FollowUp followup) {
String result;
if (followup == null)
result = null;
else
result = String.valueOf(followup.getId());
return result;
}
 }
