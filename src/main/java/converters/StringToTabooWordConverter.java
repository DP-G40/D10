package converters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.TabooWordRepository;
import domain.TabooWord;
@Component
@Transactional
public class StringToTabooWordConverter implements Converter<String, TabooWord> {
@Autowired
TabooWordRepository   taboowordRepository;
@Override
public TabooWord convert(String text) {
TabooWord result;
int id;
try {
id = Integer.valueOf(text);
result = taboowordRepository.findOne(id);
} catch (Throwable oops) {
throw new IllegalArgumentException(oops);
}
return result;
}
}
